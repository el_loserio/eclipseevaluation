# Common data for Eclipse Evaluation
# 
# Author: spd
###############################################################################

# Setting up data. Going to exclude Unclear responses from future evaluation
# but store it because we need it in one of the tables
all_eclipse<-get_data('../../data/eclipse.csv')
eclipse<-all_eclipse[! all_eclipse$bug %in% unique(all_eclipse[all_eclipse$origin_class=='Unclear',]$bug),] #$
eclipse$bug<-factor(eclipse$bug)

rachota_including_non_java<-get_data('../../data/rachota.csv')
rachota_including_non_java<-rachota_including_non_java[!is.na(rachota_including_non_java$origin_class),] #$ 
non_java<-bugs_tagged(rachota_including_non_java, 'non-java')
all_rachota<-bugs_tagged(rachota_including_non_java, 'non-java', invert=TRUE)
rachota<-all_rachota[all_rachota$origin_class!='Unclear',] #$
rachota$bug<-factor(rachota$bug)

# Rest of this is for evaluation

measures<-list(measure_tp, measure_fp, measure_fn, measure_p, measure_r, measure_f) 
names(measures)<-c('TP', 'FP', 'FN', 'P', 'R', 'F') 
rows<-c('ta', 'da')
names(rows)<-c("TA", "DA")

eclipse_evaluation<-evaluate_approaches(eclipse, rows, measures)
eclipse_fallback_evaluation<-evaluate_fallback_algorithms(eclipse, measures)
eclipse_single_evaluation<-evaluate_ta_single_result(eclipse, measures)
ta_by_class<-get_results_by_class(eclipse, 'ta')
da_by_class<-get_results_by_class(eclipse, 'da')

rachota_evaluation<-evaluate_approaches(rachota, rows, measures)
rachota_fallback_evaluation<-evaluate_fallback_algorithms(rachota, measures)

colours<-get_colours()
densities<-c(NA,20,10,20,NA)
angles<-c(NA,45,135,45,NA)
pch<-c(24,25,21)

number_of_bugs<-length(levels(all_eclipse$bug))+length(levels(rachota_including_non_java$bug))+8
fixed<-eclipse[!is.na(eclipse$age),] #$
sums<-cumsum(table(fixed$age)/nrow(fixed)) #$
fixed_in_1<-round(sums[1]*100,1)
fixed_half<-min(as.numeric(names(sums[sums>0.5])))

overall<-table(all_eclipse$origin_class)
overall<-rbind(overall, table(all_rachota$origin_class))
overall<-t(overall)
overall<-addmargins(overall, 1, list('Total'=sum))
colnames(overall)<-c("Eclipse", "Rachota")

