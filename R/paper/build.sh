#! /bin/sh

set -e

Sweave.sh eclipse-evaluation.Rnw
pdflatex eclipse-evaluation.tex
bibtex eclipse-evaluation.aux
pdflatex eclipse-evaluation.tex
pdflatex eclipse-evaluation.tex

