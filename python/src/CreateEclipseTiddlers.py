#!/usr/bin/python

'''
See LICENCE_GPL for licensing information

@author: spd
'''
from TiddlyWiki import *
from Utils import get_config
from BeautifulSoup import BeautifulStoneSoup
import bugzilla
from RandomBugs import AttrDict, XmlDictConfig
from config import config as pybugz_config
from urlparse import  urljoin
from urllib import urlencode
import re
import cgi

def get_bugs_and_commits(config):
    source = open(config['bugs'])
    parser = BeautifulStoneSoup(source, selfClosingTags=['count'])
    
    commits = {}
    bugs = {}
    unique_bugs = set()
    for commit in parser.findAll('fix'):
        file = commit.parent['filename']
        revision = commit['revision_id']
        id = revision + '|' + file
        bug_id = str(commit['bug_id'])
        unique_bugs.add(bug_id)
        
        if id in bugs:
            bugs[id].append(bug_id)
        else:
            commits[id] = commit
            bugs[id] = [bug_id]
    
    source.close()
    return commits, bugs, unique_bugs
    

def write_commits(config, out_file, commits, bugs):
    viewvc = config['viewvc']
    regex = re.compile('\d+')
    for id, commit in commits.items():
        revision, file = id.split('|')
        title = 'Commit ' + id
        print title
        write_tiddler_start(out_file, title)
        write_tags(out_file, 'commit', *['Bug ' + bug for bug in bugs[id]])
        file_link = viewvc + file
        write_attribute(out_file, 'file', format_link(file, file_link))
        revision_link = format_link(revision, file_link + '?annotate=' + revision)
        matches = regex.findall(revision)
        if(matches):
            end = int(matches[-1]) - 1
            if end:
                previous = '.'.join(matches[:-1]) + '.' + str(end)
                revision_link += ' (' + format_link('Diff', file_link + '?r1=' + previous + '&r2=' + revision) + ')'
        write_attribute(out_file, 'revision', revision_link)
        write_attribute(out_file, 'date')
        write_attribute(out_file, 'message', commit.find('message').string)
        write_content_start(out_file)
        write_tiddler_end(out_file)

def write_bugs(config, out_file, bugs):
    url = 'https://bugs.eclipse.org/bugs/'
    bugz = bugzilla.Bugz(url, skip_auth=True)
    for bug_id in bugs:
        tree = bugz.get(bug_id)
        if not tree:
            continue
        bug = AttrDict(XmlDictConfig(tree.getroot()))
        title = 'Bug ' + bug_id
        print title
        
        write_tiddler_start(out_file, title)
        write_tags(out_file, 'bug')
        summary = get_field(bug, 'short_desc')
        summary += ' ' + format_image_link('web', get_bugzilla_url(bug))
        write_attribute(out_file, 'summary', summary)
        write_attribute(out_file, 'datesubmitted', get_field(bug, 'creation_ts'))
        write_attribute(out_file, 'status', get_field(bug, 'bug_status') + ' ' + get_field(bug, 'resolution'))
        write_attribute(out_file, 'priority', get_field(bug, 'priority'))
        write_attribute(out_file, 'severity', get_field(bug, 'bug_severity'))
        write_attribute(out_file, 'category', get_field(bug, 'product') + ' ' + get_field(bug, 'component'))
        write_content_start(out_file)
        if bug.bug.get('long_desc') and 'thetext' in bug.bug.long_desc[0]:
            out_file.write('<nowiki>')
            out_file.write(cgi.escape(bug.bug.long_desc[0]['thetext']))
            out_file.write('\n')
            out_file.write('</nowiki>')
        out_file.write('\n')
        write_tiddler_end(out_file)

def get_bugzilla_url(bug):
    qparams = pybugz_config.params['show'].copy()
    if 'ctype' in qparams:
        del qparams['ctype']
    qparams['id'] = bug.bug.bug_id
    req_params = urlencode(qparams, True)
    req_url = urljoin(bug.urlbase,  pybugz_config.urls['show'])
    req_url += '?' + req_params
    return req_url

def get_field(bug, key):
    if key in bug.bug:
        return bug.bug[key]
    return ''

def write_tiddlers(config, out_file):
    commits, bugs, unique_bugs = get_bugs_and_commits(config)
    write_commits(config, out_file, commits, bugs)
    write_bugs(config, out_file, unique_bugs)

if __name__ == '__main__':
    config = get_config('config', 'eclipse')
    in_file, out_file = open_files(config, 'template', 'dest.all', True)
    create_tiddly_wiki(config, in_file, out_file, write_tiddlers)
