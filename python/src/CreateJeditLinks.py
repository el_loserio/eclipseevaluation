'''
Created on 21 Dec 2011

@author: seb10184
'''
from Utils import get_config
from collections import defaultdict
from subprocess import Popen, PIPE
from operator import attrgetter, itemgetter
from os import path, listdir
from cPickle import dump, load, HIGHEST_PROTOCOL

manual_links = {
    '1520769': 10818,
    '1574587': 9037,
    '1593526': 10817,
    '1593735': 8580,
    '1601718': 8169,
    '1641997': 8719,
    '1825720': 11429,
    '2668434': 14804,
    '2777073': 14973,
    '2068307': 13438,
}

manual_files = {
    'org.gjt.sp.jedit.options.ToolBarEditDialog': 'org/gjt/sp/jedit/options/ToolBarOptionPane.java',
    'org.gjt.sp.jedit.options.WindowTableModel': 'org/gjt/sp/jedit/options/DockingOptionPane.java'
}

class Bug(object):
    def __init__(self, bug_id):
        self.bug_id = bug_id
        self.methods = {}
        self.revisions = defaultdict(list)

class JavaClass(object):
    def __init__(self, name):
        self.bugs = set()
        self.file = 'Unknown'
        self.name = name

def read_bugs(src):
    classes = {}
    for file_name in listdir(src):
        bug_id = file_name.replace('GoldSet', '').replace('.txt', '')
        bug = Bug(bug_id)
        with open(path.join(src, file_name)) as bug_file:
            for line in bug_file:
                method_name = line.split('(')[0]
                class_name = method_name.rsplit('.', 1)[0]
                try:
                    java_class = classes[class_name]
                except KeyError:
                    java_class = JavaClass(class_name)
                    classes[class_name] = java_class
                java_class.bugs.add(bug)
                bug.methods[method_name] = java_class
    
    return classes

def parse_svn_logs(classes, src):
    base_args = []
    base_args += ['svn']
    base_args += ['log']
    base_args += ['-g']
    base_args += ['-r']
    base_args += ['5111:16702']
    
    for java_class in classes.values():
        class_name = java_class.name
        while class_name:
            if class_name in manual_files:
                file_name = manual_files[class_name]
            else:
                file_name = class_name.replace('.', '/') + '.java'
                
            args = base_args + [file_name]
            stdout, stderr = Popen(args, cwd=src, stdout=PIPE, stderr=PIPE).communicate()
            if stderr:
                print stderr
                parts = class_name.rsplit('.', 1)
                if len(parts) == 1:
                    break
                
                class_name = parts[0]
                continue
            
            java_class.file = file_name
            new_revision = True
            for line in stdout.splitlines():
                if line.startswith('---------'):
                    new_revision = True
                    continue
                
                if new_revision and line.startswith('r'):
                    revision = line.split(None,1)[0]
                    revision = revision.replace('r', '')
                    new_revision = False
                    continue
                
                for bug in java_class.bugs:
                    if bug.bug_id in line:
                        bug.revisions[java_class] += [(revision, line)]
                
                new_revision = False
            break
        
def write_output(config, classes):
    bugs = set()
    for java_class in classes.values():
        bugs.update(java_class.bugs)
    
    bug_ids = get_issue_types(config, 'bugs')
    feature_ids = get_issue_types(config, 'features')
    patch_ids = get_issue_types(config, 'patches')
    
    with open(config['out'], 'w') as out:
        out.write('<html>')
        for bug in sorted(bugs, key=attrgetter('bug_id')):
            all_revisions = defaultdict(list)
            for method, java_class in bug.methods.items():
                revisions = bug.revisions.get(java_class)
                if revisions:
                    for revision in revisions:
                        all_revisions[revision] += [(method, java_class)]
                else:
                    all_revisions['Not found'] += [(method, java_class)]
            
            bugz = '<a href="http://sourceforge.net/tracker/?func=detail&aid=%s&group_id=588&atid=%s">Bugzilla</a>'
            bugzilla = 'Not found'
            if bug.bug_id in bug_ids:
                bugzilla = bugz % (bug.bug_id, 100588)
            elif bug.bug_id in feature_ids:
                bugzilla = bugz % (bug.bug_id, 350588)
            elif bug.bug_id in patch_ids:
                bugzilla = bugz % (bug.bug_id, 300588)
            out.write('<h3>Bug %s [%s]</h3>\n' % (bug.bug_id, bugzilla))
            
            for revision, methods in sorted(all_revisions.items(), key=itemgetter(1)):
                methods.sort()
                if isinstance(revision, tuple):
                    out.write('<h4>r%s - %s [<a href="http://jedit.svn.sourceforge.net/viewvc/jedit?view=revision&revision=%s">ViewVC</a>]</h4>\n' % (revision[0], revision[1], revision[0]))
                elif bug.bug_id in manual_links:
                    
                    out.write('<h4>r%s - Found manually [<a href="http://jedit.svn.sourceforge.net/viewvc/jedit?view=revision&revision=%s">ViewVC</a>]</h4>\n' % (manual_links[bug.bug_id], manual_links[bug.bug_id]))
                else:
                    out.write('<h4>%s</h4>\n' % (revision, ))
                last_class = None
                for method, java_class in methods:
                    out.write('%s (%s)' % (method, java_class.file))
                    if java_class != last_class:
                        out.write(' [<a href="http://jedit.svn.sourceforge.net/viewvc/jedit/jEdit/trunk/%s?view=log' % (java_class.file,))
                        if isinstance(revision, tuple):
                            out.write('&pathrev=%s' % (revision[0],))
                        out.write('">ViewVC</a>]')
                        last_class = java_class
                    out.write('<br/>\n')
       
        out.write('</html>')

def get_issue_types(config, key):
    with open(config[key]) as f:
        return [l.strip() for l in f]

if __name__ == '__main__':
    config = get_config('config', 'jedit')
    
    classes = read_bugs(config['fixes'])        
    parse_svn_logs(classes, config['src'])
    
    with open(config['save_file'], 'wb') as save_file:
        dump(classes, save_file, HIGHEST_PROTOCOL)
    
    with open(config['save_file'], 'rb') as save_file:
        classes = load(save_file)
    
    write_output(config, classes)