'''
Created on 14 Dec 2012

@author: seb10184
'''

from collections import defaultdict
from operator import itemgetter
from random import sample, shuffle
from TiddlyWiki import read_tiddlers
from Utils import get_config, seed_random

def write_bugs(bugs, out_file, bugzilla):
    with open(out_file, 'w') as o:
        o.write('<html>')
        bugs.sort(key=itemgetter(0))
        for bug, commits in bugs:
            bug_id = bug.rsplit(None, 1)[1]
            url = bugzilla % (bug_id)
            o.write('<h3>Bug %s [<a href="%s">Bugzilla</a>]</h3>\n' % (bug_id, url))
            
            commits.sort(key=itemgetter('filename', 'revision'))
            for commit in commits:
                o.write('<h4>%s: %s [<a href="%s">ViewVC</a>]</h4>\n' % (commit['filename'], commit['revision'], commit['diff']))
                o.write('Message: ' + commit['message'] + '<br/>')
                o.write('Origin: ' + commit['origin'] + '<br/>')
                o.write('TA: ' + commit['ta'] + '<br/>')
                o.write('DA: ' + commit['da'] + '<br/>')

def write_all_bugs(project):
    config = get_config('config', project)
    in_file = config['dest.sample']
    with open(in_file) as wiki:
        tiddlers = read_tiddlers(wiki)

    bugs = defaultdict(list)
    for tiddler in tiddlers:
        try:
            if '[[Bug' in tiddler['tags']:
                commit = {}
                commit['filename'], _ = tiddler['file'].strip('[]').split('|', 1)
                revision, _, diff = tiddler['revision'].partition('(')
                commit['revision'], commit['diff'] = revision.strip('[]').split('|', 1)
                if diff:
                    _, commit['diff'] = diff.strip('()[]').split('|', 1)
                commit['diff'] = commit['diff'].replace(config['replication.viewvc.old'], config['replication.viewvc.new'])
                commit['origin'], _, _ = tiddler['origin'].strip('[]').partition('|')
                commit['message'] = tiddler['message']
                commit['ta'] = tiddler['taresult']
                commit['da'] = tiddler['daresult']
                tag_value = tiddler['tags']
                start = tag_value.find('[[')
                while start > -1:
                    start += 2
                    end = tag_value.find(']]', start)
                    tag = tag_value[start:end]
                    if tag.startswith('Bug'):
                        bugs[tag].append(commit)
                    tag_value = tag_value[end:]
                    start = tag_value.find('[[')
        except KeyError as e:
            print 'Error for tiddler', tiddler['title'], e
            pass

    bugs = bugs.items()

    seed = seed_random(config)
    print 'Seed used', seed

    sample_a = int(config['replication.sample.a'])
    sample_b = int(config['replication.sample.b'])
    sample_common = int(config['replication.sample.common'])
    sample_size = sample_a + sample_b + sample_common
    bugs = sample(bugs, sample_size)
    shuffle(bugs)

    for bug, _ in bugs[sample_a:sample_a+sample_common]:
        print 'Common bug', bug
    write_bugs(bugs[:sample_a+sample_common], config['replication.out.a'], config['replication.bugzilla'])
    write_bugs(bugs[sample_a:], config['replication.out.b'], config['replication.bugzilla'])

if __name__ == '__main__':
    write_all_bugs('eclipse')
    write_all_bugs('rachota')
