'''
See LICENCE_BSD for licensing information

@author: spd
'''

import codecs
import re
from TiddlyWiki import read_tiddlers, parse_tags
from Csv import write
from Utils import get_config

ignore_tags = ['daplus-', 'da-', 'ta-', 'originclass-', 'commit', '/', 'match-']

def sort_tags(tags, bugs):
    sorted = {}
    sorted['tags'] = []
    for tag in tags:
        if tag.startswith('Bug '):
            if tag in bugs:
                sorted['bug'] = tag.replace('Bug ', '')
                sorted['type'] = bugs[tag] 
        elif not any((tag.startswith(t) for t in ignore_tags)):
            sorted['tags'].append(tag)
    return sorted

def get_revision_file(commit):
    revision, file = commit['title'].split('|')
    return get_version_list(revision), file

def get_version_list(versions):
    if versions:
        matches = re.findall('\d+(?:\.\d+)+', versions)
        matches = set(matches)
        if not matches:
            return None
        return '|'.join(matches)
    return None

def write_results(in_file, out_file):
    divs = read_tiddlers(in_file)
    bugs = {}
    for div in divs:
        tags = parse_tags(div.get('tags'))
        if 'bug' in tags:
            type = div.get('type')
            if not type:
                type = 'Bug'
            bugs[div['title']] = type
            
    write(out_file, 'File')
    write(out_file, 'Version')
    write(out_file, 'Issue ID')
    write(out_file, 'Issue Type')
    write(out_file, 'Origin(s)')
    write(out_file, 'TA Result(s)')
    write(out_file, 'DA Result(s)')
    write(out_file, 'Origin Class')
    write(out_file, 'TA Most Frequent')
    write(out_file, 'TA Most Recent')
    write(out_file, 'Tags\n', False)
    
    for div in divs:
        tags = parse_tags(div.get('tags'))
        if 'commit' in tags:
            revision, file = get_revision_file(div)
            sorted = sort_tags(tags, bugs)
            
            write(out_file, file)
            write(out_file, revision)
            write(out_file, sorted['bug'])
            write(out_file, sorted['type'])
            write(out_file, get_version_list(div.get('origin')), na='?')
            write(out_file, get_version_list(div.get('taresult')), na='?')
            write(out_file, get_version_list(div.get('daresult')), na='?')
            write(out_file, div.get('originclass'), na='?')
            write(out_file, get_version_list(div.get('frequent')), na='?')
            write(out_file, get_version_list(div.get('recent')), na='?')
            write(out_file, '|'.join(sorted['tags']), False)
            write(out_file, '\n', False)
    in_file.close()

if __name__ == '__main__':
    config = get_config('config', 'eclipse')
    in_file = codecs.open(config['dest.sample'], 'r', 'utf-8')
    out_file = open(config['eclipse.results'], 'w')
    write_results(in_file, out_file)

    in_file = codecs.open(config['rachota.source'], 'r', 'utf-8')
    out_file = open(config['rachota.results'], 'w')
    write_results(in_file, out_file)
